# Build Gitlab runner with packer and terraform

This repository contains the code to build a Gitlab runner with packer and terraform.

## Requirements

- [Packer](https://www.packer.io/)
- [Terraform](https://www.terraform.io/)

## Build

To build the image, run the following command:

```bash
packer build packer/config.pkr.hcl
```