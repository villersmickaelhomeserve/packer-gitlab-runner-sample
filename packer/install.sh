#!/bin/bash
set -x

export DEBIAN_FRONTEND=noninteractive

# Update and install system
sudo apt-get update
sudo apt-get upgrade -y

# Install Docker
sudo apt-get install -y curl ca-certificates curl gnupg
sudo install -m 0755 -d /etc/apt/keyrings
curl -fsSL https://download.docker.com/linux/debian/gpg | sudo gpg --dearmor -o /etc/apt/keyrings/docker.gpg
sudo chmod a+r /etc/apt/keyrings/docker.gpg
echo \
  "deb [arch="$(dpkg --print-architecture)" signed-by=/etc/apt/keyrings/docker.gpg] https://download.docker.com/linux/debian \
  "$(. /etc/os-release && echo "$VERSION_CODENAME")" stable" | \
  sudo tee /etc/apt/sources.list.d/docker.list > /dev/null
sudo apt-get update
sudo apt-get install docker-ce docker-ce-cli containerd.io docker-buildx-plugin docker-compose-plugin -y

# Download and install Gitlab Runner
curl -LJO "https://gitlab.com/gitlab-org/gitlab-runner/-/releases/${GITLAB_RUNNER_VERSION}/downloads/packages/deb/gitlab-runner_arm64.deb"
sudo dpkg -i gitlab-runner_amd64.deb

# Register Gitlab Runner
sudo gitlab-runner register --non-interactive --url https://gitlab.com/ --registration-token ${GITLAB_RUNNER_TOKEN} --executor shell --description gitlab-runner

# Start Gitlab Runner
sudo gitlab-runner start