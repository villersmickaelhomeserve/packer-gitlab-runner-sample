packer {
  required_plugins {
    googlecompute = {
      version = "~> v1.1"
      source  = "github.com/hashicorp/googlecompute"
    }
  }
}

locals {
  timestamp = regex_replace(timestamp(), "[- TZ:]", "")
  runners = {
    devops = {
      token = "TOKEN1"
    },
  }
}

variable "project_id" {
  type    = string
  default = "gitlab-runner-189408"
}

source "googlecompute" "devops" {
  project_id          = var.project_id
  zone                = "europe-west1-b"
  ssh_username        = "packer"
  image_name          = "gitlab-devops-${local.timestamp}"
  source_image_family = "debian-11"
  source_image        = "debian-11-bullseye-v20230509"
  image_description   = "Image with gitlab-runner preinstalled for devops"
  machine_type        = "n1-standard-1"
  # add network
  network    = "network-runners"
  subnetwork = "subnet1"
  tags       = ["snmp-homeserve", "ssh-homeserve"]
  disk_size  = 20
  image_labels = {
    os_version  = "debian-11-bullseye-v20230509"
    application = "gitlab-runner"
  }
}

build {
  sources = [
    "sources.googlecompute.devops"
  ]

  provisioner "shell" {
    env = {
      GITLAB_RUNNER_VERSION = "v15.11.0"
    }
    inline = [
      "printenv",
    ]
    override = {
      devops = {
        env = {
          GITLAB_RUNNER_TOKEN = local.runners.devops.token
        }
      }
    }
  }
  /*provisioner "shell" {
    script = "install.sh"
  }*/
}



